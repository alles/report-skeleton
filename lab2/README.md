# Лабораторная работа 2

**Название:** "Разработка драйверов блочных устройств"

**Цель работы:** получить знания и навыки разработки драйверов блочных устройств для операционной системы Linux.  

## Описание функциональности драйвера

Создает один первичный раздел размером 30Мбайт и один расширенный раздел, содержащий два логических раздела размером 10Мбайт каждый.  

## Инструкция по сборке

`make`

## Инструкция пользователя

`insmod main.ko`

## Примеры использования


```
# fdisk -l
Disk /dev/sda: 20 GiB, 21474836480 bytes, 41943040 sectors
Units: sectors of 1 * 512 = 512 bytes
Sector size (logical/physical): 512 bytes / 512 bytes
I/O size (minimum/optimal): 512 bytes / 512 bytes
Disklabel type: dos
Disk identifier: 0x2b2f3388

Device     Boot Start      End  Sectors Size Id Type
/dev/sda1  *     2048 41940991 41938944  20G 83 Linux


Disk /dev/mydisk: 322,8 MiB, 338493440 bytes, 661120 sectors
Units: sectors of 1 * 512 = 512 bytes
Sector size (logical/physical): 512 bytes / 512 bytes
I/O size (minimum/optimal): 512 bytes / 512 bytes
Disklabel type: dos
Disk identifier: 0x36e5756d

Device       Boot Start    End Sectors Size Id Type
/dev/mydisk1          1  61440   61440  30M 83 Linux
/dev/mydisk2      61441 102400   40960  20M  5 Extended
/dev/mydisk5      61442  81920   20479  10M 83 Linux
/dev/mydisk6      81922 102400   20479  10M 83 Linux

```

```
# dd if=/dev/mydisk1 of=/dev/mydisk5 bs=512 count=20000
20000+0 records in
20000+0 records out
10240000 bytes (10 MB, 9,8 MiB) copied, 0,160487 s, 63,8 MB/s
```

```
# dd if=/dev/mydisk1 of=/dev/sda1 bs=512 count=20000
20000+0 records in
20000+0 records out
10240000 bytes (10 MB, 9,8 MiB) copied, 0,108125 s, 94,7 MB/s
```

#include <linux/module.h>
#include <linux/version.h>
#include <linux/netdevice.h>
#include <linux/etherdevice.h>
#include <linux/moduleparam.h>
#include <linux/in.h>
#include <net/arp.h>
#include <linux/ip.h>
#include <linux/udp.h>
#include <linux/proc_fs.h>

static char* link = "enp0s3";
module_param(link, charp, 0);

static char* ifname = "vni%d";
static unsigned char data[1500];

static struct net_device_stats stats;

static struct net_device *child = NULL;
struct priv {
    struct net_device *parent;
};

static struct proc_dir_entry* entry;
int checked_count = 0;
int dest_port = 630;
static int src_ports[1000];
static int dest_ports[1000];
int tx_flag = 0;

static char check_frame(struct sk_buff *skb, unsigned char data_shift) {
	unsigned char *user_data_ptr = NULL;
    struct iphdr *ip = (struct iphdr *)skb_network_header(skb);
    struct udphdr *udp = NULL;
    int data_len = 0;

	if (IPPROTO_UDP == ip->protocol) {
        udp = (struct udphdr*)((unsigned char*)ip + (ip->ihl * 4));
        int dest = ntohs(udp->dest);
        if (dest == dest_port) {
            src_ports[checked_count] = ntohs(udp->source);
            dest_ports[checked_count] = ntohs(udp->dest);
            checked_count++;
            printk("Src port: %d \nDst port: %d\n", ntohs(udp->source), ntohs(udp->dest));

            data_len = ntohs(udp->len) - sizeof(struct udphdr);
            user_data_ptr = (unsigned char *)(skb->data + sizeof(struct iphdr)  + sizeof(struct udphdr)) + data_shift;
            memcpy(data, user_data_ptr, data_len);
            data[data_len] = '\0';
            
            printk(KERN_INFO "##Checked pakets: %u", checked_count);
            printk("Captured UDP datagram, saddr: %d.%d.%d.%d\n",
                   ntohl(ip->saddr) >> 24, (ntohl(ip->saddr) >> 16) & 0x00FF,
                   (ntohl(ip->saddr) >> 8) & 0x0000FF, (ntohl(ip->saddr)) & 0x000000FF);
            printk("daddr: %d.%d.%d.%d\n",
                ntohl(ip->daddr) >> 24, (ntohl(ip->daddr) >> 16) & 0x00FF,
                (ntohl(ip->daddr) >> 8) & 0x0000FF, (ntohl(ip->daddr)) & 0x000000FF);

    	    printk(KERN_INFO "Data length: %d. Data:%s", data_len, data);
            //printk("%s", data);         
        } else {
            printk(KERN_INFO "Another port: %u", dest);
        }
        
        return 1;

    }

    if (tx_flag == 1) {
        printk(KERN_INFO "Another protocol: %u", ip->protocol);
    }
    return 0;
}

static rx_handler_result_t handle_frame(struct sk_buff **pskb) { //вызывается при приёме
   // if (child) {
        
        	if (check_frame(*pskb, 0)) {
                stats.rx_packets++;
                stats.rx_bytes += (*pskb)->len;
            }
        (*pskb)->dev = child;
        return RX_HANDLER_ANOTHER;
    //}   
    return RX_HANDLER_PASS; 
} 

static int open(struct net_device *dev) {
    netif_start_queue(dev); //Пакеты не передаются, пока не была вызвана netif_start_queue.
    printk(KERN_INFO "%s: device opened", dev->name);
    return 0; 
} 

static int stop(struct net_device *dev) {
    netif_stop_queue(dev); //netif_stop_queue приостанавливает передачу
    printk(KERN_INFO "%s: device closed", dev->name);
    return 0; 
} 

static netdev_tx_t start_xmit(struct sk_buff *skb, struct net_device *dev) { //вызывается при отправке данных
    struct priv *priv = netdev_priv(dev);
    
    tx_flag = 1;
    if (check_frame(skb, 14)) {
        stats.tx_packets++;
        stats.tx_bytes += skb->len;
    }
    tx_flag = 0;

    printk(KERN_INFO "########transmit", dev->name);
    if (priv->parent) {
        skb->dev = priv->parent;
        skb->priority = 1;
        dev_queue_xmit(skb);
        return 0;
    }
    return NETDEV_TX_OK;
}

static struct net_device_stats *get_stats(struct net_device *dev) {
    return &stats;
} 

static struct net_device_ops net_device_ops = {
    .ndo_open = open,
    .ndo_stop = stop,
    .ndo_get_stats = get_stats,
    .ndo_start_xmit = start_xmit
};

static void setup(struct net_device *dev) {
    int i;
    ether_setup(dev); 
    memset(netdev_priv(dev), 0, sizeof(struct priv)); //netdev_priv - Get network device private data
    dev->netdev_ops = &net_device_ops;

    //fill in the MAC address
    for (i = 0; i < ETH_ALEN; i++)
        dev->dev_addr[i] = (char)i;
} 

static ssize_t proc_read(struct file *f, char __user *buf, size_t len, loff_t *off) {
    printk(KERN_INFO "###### %s \n",__FUNCTION__);
    int ret, cnt = 0;
    char data[1024],*ptr;
    ptr = data;

    int i;
    if (checked_count > 0) {
        for (i = 0; i < checked_count; i++) {
            cnt += sprintf(&ptr[cnt],"Packet %u: source port %d, destination port %d\n", i+1, src_ports[i], dest_ports[i]);
        }
        ret = simple_read_from_buffer(buf, len, off, data, strlen(data));
    } else {
        cnt += sprintf(&ptr[cnt],"No packets\n");
        ret = simple_read_from_buffer(buf, len, off, data, strlen(data));
    }
    return ret;
}

static struct file_operations proc_fops = {
  .read = proc_read,
};


int __init vni_init(void) {
    entry = proc_create("var3", 0666, NULL, &proc_fops);
    if (entry == NULL) {
		  return -1;
    }

    int err = 0;
    struct priv *priv;
    child = alloc_netdev(sizeof(struct priv), ifname, NET_NAME_UNKNOWN, setup);
    if (child == NULL) {
        printk(KERN_ERR "%s: allocate error", THIS_MODULE->name);
        return -ENOMEM;
    }
    priv = netdev_priv(child);
    priv->parent = __dev_get_by_name(&init_net, link); //parent interface
    if (!priv->parent) {
        printk(KERN_ERR "%s: no such net: %s", THIS_MODULE->name, link);
        free_netdev(child);
        return -ENODEV;
    }
    if (priv->parent->type != ARPHRD_ETHER && priv->parent->type != ARPHRD_LOOPBACK) { //ARP protocol HARDWARE identifiers: Ethernet 10Mbps, Loopback interface 
        printk(KERN_ERR "%s: illegal net type", THIS_MODULE->name); 
        free_netdev(child);
        return -EINVAL;
    }

    //copy IP, MAC and other information
    memcpy(child->dev_addr, priv->parent->dev_addr, ETH_ALEN); //ETH_ALEN - 6 (octets in ethernet addr)
    memcpy(child->broadcast, priv->parent->broadcast, ETH_ALEN);
    if ((err = dev_alloc_name(child, child->name))) {
        printk(KERN_ERR "%s: allocate name, error %i", THIS_MODULE->name, err);
        free_netdev(child);
        return -EIO;
    }

    register_netdev(child);
    rtnl_lock();
    netdev_rx_handler_register(priv->parent, &handle_frame, NULL);
    rtnl_unlock();
    printk(KERN_INFO "Module %s loaded", THIS_MODULE->name);
    printk(KERN_INFO "%s: create link %s", THIS_MODULE->name, child->name);
    printk(KERN_INFO "%s: registered rx handler for %s", THIS_MODULE->name, priv->parent->name);
    return 0; 
}

void __exit vni_exit(void) {
    struct priv *priv = netdev_priv(child);
    if (priv->parent) {
        rtnl_lock();
        netdev_rx_handler_unregister(priv->parent);
        rtnl_unlock();
        printk(KERN_INFO "%s: unregister rx handler for %s", THIS_MODULE->name, priv->parent->name);
    }
    unregister_netdev(child);
    free_netdev(child);
    proc_remove(entry);
    printk(KERN_INFO "Module %s unloaded", THIS_MODULE->name); 
} 

module_init(vni_init);
module_exit(vni_exit);

MODULE_AUTHOR("Author");
MODULE_LICENSE("GPL");
MODULE_DESCRIPTION("Description");

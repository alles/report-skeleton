# Лабораторная работа 3

**Название:** "Разработка драйверов сетевых устройств"

**Цель работы:** получить знания и навыки разработки драйверов сетевых интерфейсов для операционной системы Linux.

## Описание функциональности драйвера

Драйвер создает виртуальный сетевой интерфейс, перехватывающий пакеты с задаваемого интерфейса. Перехватываются пакеты протокола UDP, адресуемые на конкретный порт. Состояние разбора пакетов выводится в /proc/var3

## Инструкция по сборке

`$ make`

## Инструкция пользователя

```
lo
$ sudo insmod virt_net_if.ko link=lo
$ nc -u localhost 630
$ cat /proc/var3
$ dmesg

enp0s3
$ sudo insmod virt_net_if.ko
$ echo message | sudo socat -u - udp-datagram:255.255.255.255:630,bind=:111,reuseport,broadcast
$ cat /proc/var3
$ dmesg
```


## Примеры использования
**1) Перехват приема по localhost**  
`$ ifconfig`
```
enp0s3: flags=4163<UP,BROADCAST,RUNNING,MULTICAST>  mtu 1500
        inet 192.168.0.108  netmask 255.255.255.0  broadcast 192.168.0.255
        inet6 fe80::2d88:de62:2e07:4acd  prefixlen 64  scopeid 0x20<link>
        ether 08:00:27:c8:fd:2d  txqueuelen 1000  (Ethernet)
        RX packets 24070  bytes 34555515 (34.5 MB)
        RX errors 0  dropped 0  overruns 0  frame 0
        TX packets 5208  bytes 380660 (380.6 KB)
        TX errors 0  dropped 0 overruns 0  carrier 0  collisions 0

lo: flags=73<UP,LOOPBACK,RUNNING>  mtu 65536
        inet 127.0.0.1  netmask 255.0.0.0
        inet6 ::1  prefixlen 128  scopeid 0x10<host>
        loop  txqueuelen 1000  (Local Loopback)
        RX packets 151  bytes 15583 (15.5 KB)
        RX errors 0  dropped 0  overruns 0  frame 0
        TX packets 151  bytes 15583 (15.5 KB)
        TX errors 0  dropped 0 overruns 0  carrier 0  collisions 0

vni0: flags=4163<UP,BROADCAST,RUNNING,MULTICAST>  mtu 1500
        ether 00:00:00:00:00:00  txqueuelen 1000  (Ethernet)
        RX packets 0  bytes 0 (0.0 B)
        RX errors 0  dropped 0  overruns 0  frame 0
        TX packets 0  bytes 0 (0.0 B)
        TX errors 0  dropped 0 overruns 0  carrier 0  collisions 0
```
`$ nc -u localhost 630`  
`>>> test`

`$ cat /proc/var3`
```
Packet 1: source port 39061, destination port 630
```

`$ dmesg`
```
[ 1284.551108] Module virt_net_if loaded
[ 1284.551109] virt_net_if: create link vni0
[ 1284.551110] virt_net_if: registered rx handler for lo
[ 1284.917299] vni0: device opened
[ 1331.691774] Src port: 39061 
               Dst port: 630
[ 1331.691776] ##Checked pakets: 1
[ 1331.691777] Captured UDP datagram, saddr: 127.0.0.1
[ 1331.691777] daddr: 127.0.0.1
[ 1331.691778] Data length: 5. Data:test
[ 1357.788555] ###### proc_read 
[ 1357.788874] ###### proc_read
```


**2) Перехват приема по enp0s3**  
`echo testmsg | socat -u - udp-datagram:255.255.255.255:630,bind=:635,reuseport,broadcast`  
`$ cat /proc/var3`
```
Packet 1: source port 635, destination port 630
```

`$ dmesg`
```
[  879.362996] vni0: device opened
[  938.286371] Src port: 635 
               Dst port: 630
[  938.286372] ##Checked pakets: 1
[  938.286373] Captured UDP datagram, saddr: 10.0.2.15
[  938.286373] daddr: 255.255.255.255
[  938.286374] Data length: 8. Data:testmsg
[  963.444765] ###### proc_read 
[  963.444771] ###### proc_read 
```
**3) Перехват передачи виртуального интерфейса**  
`$ ifconfig vni0 192.168.50.1` - назначение IP-адреса виртуальному интерфейсу для передачи с него  
Имеется две виртуальных машины, объединенных в сеть NAT  
`$ traceroute -i vni0 10.0.2.5 -p 630` - передача UDP на вторую машину  
`$ cat /proc/var3` - просмотр результата на первой машине   
```
Packet 1: source port 40790, destination port 630
```

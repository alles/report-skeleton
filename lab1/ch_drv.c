#include <linux/module.h>
#include <linux/version.h>
#include <linux/kernel.h>
#include <linux/types.h>
#include <linux/kdev_t.h>
#include <linux/fs.h>
#include <linux/device.h>
#include <linux/cdev.h>
#include <linux/init.h>
#include <linux/proc_fs.h>
#include <linux/string.h>
#include <linux/uaccess.h>

#define BUFSZ 512

u32 result[BUFSZ];
size_t resultpos = 0;

static dev_t first;
static struct cdev c_dev; 
static struct class *cl;
static struct proc_dir_entry* entry;

static ssize_t dev_read(struct file *f, char __user *buf, size_t len, loff_t *off)
{
	size_t i;
	for (i = 0; i < resultpos; i++) {
		printk(KERN_INFO "%d \n", result[i]);
	}
    return 0;
}

static ssize_t dev_write(struct file *f, const char __user *buf,  size_t len, loff_t *off)
{
    printk(KERN_INFO "###### %s \n",__FUNCTION__);
    int number = 0;
    int sum = 0;
    size_t i;
    for(i = 0; i < len; i++) {
        if (buf[i] >='0' && buf[i]<='9') {
            number = 10*number + (buf[i] - '0');
            if (i == len-1) {
                sum += number;
            }
        }
        else {
            sum += number;
            number = 0;
        }
    }
    result[resultpos] = sum;
    resultpos++;
  	return len;
}

static struct file_operations mychdev_fops =
{
  .read = dev_read,
  .write = dev_write
};
 

static ssize_t proc_write(struct file *f, const char __user * buf, size_t len, loff_t* off) 
{
    printk(KERN_INFO "###### %s \n",__FUNCTION__);
	return len;
}

static ssize_t proc_read(struct file *f, char __user *buf, size_t len, loff_t *off) 
{
    printk(KERN_INFO "###### %s \n",__FUNCTION__);
    size_t i;
    int ret, cnt;
    char data[BUFSZ],*ptr;
    cnt = 0;
    ptr = data;
    for(i = 0; i < resultpos; i++) {
    	cnt += sprintf(&ptr[cnt],"%d\n", result[i]);
    }
    ret = simple_read_from_buffer(buf, len, off, data, strlen(data));
    return ret;
}

static struct file_operations proc_fops =
{
  .read = proc_read,
  .write = proc_write
};
 
static int mychardev_uevent(struct device *dev, struct kobj_uevent_env *env)
{
    add_uevent_var(env, "DEVMODE=%#o", 0666);
    return 0;
}

static int __init ch_drv_init(void)
{
    if (alloc_chrdev_region(&first, 0, 1, "ch_dev") < 0)
	  {
		return -1;
	  }
    if ((cl = class_create(THIS_MODULE, "chardrv")) == NULL)
	  {
		unregister_chrdev_region(first, 1);
		return -1;
	  }
	  cl->dev_uevent = mychardev_uevent;  
    if (device_create(cl, NULL, first, NULL, "var3") == NULL)
	  {
		class_destroy(cl);
		unregister_chrdev_region(first, 1);
		return -1;
	  }
    cdev_init(&c_dev, &mychdev_fops);
    if (cdev_add(&c_dev, first, 1) == -1)
	  {
		device_destroy(cl, first);
		class_destroy(cl);
		unregister_chrdev_region(first, 1);
		return -1;
	  }


    entry = proc_create("var3", 0666, NULL, &proc_fops);
    if (entry == NULL) {
    	device_destroy(cl, first);
		  class_destroy(cl);
		  unregister_chrdev_region(first, 1);
		  return -1;
    }
    return 0;
}
 
static void __exit ch_drv_exit(void)
{
    cdev_del(&c_dev);
    device_destroy(cl, first);
    class_destroy(cl);
    unregister_chrdev_region(first, 1);
    proc_remove(entry);
}
 
module_init(ch_drv_init);
module_exit(ch_drv_exit);
 
MODULE_LICENSE("GPL");
MODULE_DESCRIPTION("The first kernel module");


